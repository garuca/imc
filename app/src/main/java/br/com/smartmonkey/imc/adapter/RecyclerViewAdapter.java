package br.com.smartmonkey.imc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;


import br.com.smartmonkey.imc.R;
import br.com.smartmonkey.imc.holder.RecyclerViewHolders;
import br.com.smartmonkey.imc.model.BodyMassIndex;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {

    private List<BodyMassIndex> BMIs;
    private Context context;

    public RecyclerViewAdapter(Context context, List<BodyMassIndex> BMIs) {
        this.BMIs = BMIs;
        this.context = context;
    }


    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        NumberFormat formatarFloat = new DecimalFormat("#.#");

        holder.textViewBMI.setText(formatarFloat.format(BMIs.get(position).calculateBMI()));
        holder.textViewHeight.setText(formatarFloat.format(BMIs.get(position).getHeight())+" m");
        holder.textVieWeight.setText(formatarFloat.format(BMIs.get(position).getWeight())+" Kg");

        holder.textViewVariation.setText(formatarFloat.format(position==0?BMIs.get(position).getWeight():BMIs.get(position).calculateVariation(BMIs.get(position-1)))+" Kg");
        if (position!=0) {
            if (BMIs.get(position).calculateVariation(BMIs.get(position - 1)) < 0) {
                holder.textViewVariation.setTextColor(context.getResources().getColor(R.color.green));
            } else {
                holder.textViewVariation.setTextColor(context.getResources().getColor(R.color.red));
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.BMIs.size();
    }
}
