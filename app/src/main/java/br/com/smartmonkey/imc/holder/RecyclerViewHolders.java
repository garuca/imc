package br.com.smartmonkey.imc.holder;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import br.com.smartmonkey.imc.R;


public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView textViewBMI;
    public TextView textVieWeight;
    public TextView textViewHeight;
    public TextView textViewVariation;

    public RecyclerViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        textViewBMI = (TextView)itemView.findViewById(R.id.textViewBMI);
        textVieWeight = (TextView)itemView.findViewById(R.id.textVieWeight);
        textViewHeight = (TextView)itemView.findViewById(R.id.textViewHeight);
        textViewVariation = (TextView)itemView.findViewById(R.id.textViewVariation);

    }

    @Override
    public void onClick(View view) {

    }
}