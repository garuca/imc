package br.com.smartmonkey.imc.activiy;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.smartmonkey.imc.R;
import br.com.smartmonkey.imc.adapter.RecyclerViewAdapter;
import br.com.smartmonkey.imc.controller.BMIControler;
import br.com.smartmonkey.imc.model.BodyMassIndex;

public class MainActivity extends AppCompatActivity {
    private GridLayoutManager layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        refreshList();
    }

    public void refreshList(){
        List<BodyMassIndex> bodyMassIndexList = new BMIControler().getBMIs(MainActivity.this);
        layout = new GridLayoutManager(this, 1);

        RecyclerView rView = (RecyclerView) findViewById(R.id.recycler_view);
        rView.setLayoutManager(layout);
        RecyclerViewAdapter rcAdapter = new RecyclerViewAdapter(MainActivity.this, bodyMassIndexList);
        rView.setAdapter(rcAdapter);

    }

    static boolean FAMALE = false;
    static boolean MALE = true;
    boolean gendeer = MALE;

    public void showDialog (){

        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);

        View promptView = layoutInflater.inflate(R.layout.dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);

        alertDialogBuilder.setView(promptView);

        RadioButton radioButton = (RadioButton) promptView.findViewById(R.id.male);
        radioButton.setChecked(true);
        RadioGroup radioGroupGendeer = (RadioGroup) promptView.findViewById(R.id.radioGroupGendeer);

        radioGroupGendeer.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(checkedId == R.id.male) {
                    gendeer = MALE;
                } else{
                    gendeer = FAMALE;
                }
            }

        });
        final EditText editTextHeight = (EditText) promptView.findViewById(R.id.editTextHeight);
        final EditText editTextWeight = (EditText) promptView.findViewById(R.id.editTextWeight);



        alertDialogBuilder
                .setCancelable(false).setTitle(getResources().getString(R.string.info_the_data))

                .setPositiveButton(getResources().getString(R.string.finsh), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(editTextHeight.getText().toString()==null ||editTextHeight.getText().toString().equals("")){


                        }else if(editTextWeight.getText().toString()==null ||editTextWeight.getText().toString().equals("")){


                        }else {
                            float height = Float.parseFloat(editTextHeight.getText().toString());
                            float weight = Float.parseFloat(editTextWeight.getText().toString());
                            new BMIControler().save(MainActivity.this,new BodyMassIndex(height,weight,gendeer));
                            refreshList();
                        }

                    }
                })
                .setNegativeButton(getResources().getString(R.string.back),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });


        AlertDialog alertD = alertDialogBuilder.create();
        alertD.show();



    }
}
