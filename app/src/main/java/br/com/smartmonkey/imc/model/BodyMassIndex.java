package br.com.smartmonkey.imc.model;

/**
 * Created by Gabriel on 28/07/2016.
 */
public class BodyMassIndex {
    float height;
    float weight;
    boolean gender;

    public BodyMassIndex() {
    }

    public BodyMassIndex(float height, float weight, boolean gender) {
        this.height = height;
        this.weight = weight;
        this.gender = gender;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public boolean isGender() {
        return gender;
    }

    public void setSex(boolean sex) {
        this.gender = sex;
    }
    public float calculateBMI(){
        return (float) (this.weight/Math.pow(this.height, 2));
    }
    public float calculateVariation(BodyMassIndex BMIBefore){
        return -(BMIBefore.weight-this.weight);
    }



    @Override
    public String toString() {
        return "BodyMassIndex{" +
                "height=" + height +
                ", weight=" + weight +
                ", gender=" + gender +
                ", BMI=" + calculateBMI() +
                '}';
    }
}
