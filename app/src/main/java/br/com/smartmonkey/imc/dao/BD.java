package br.com.smartmonkey.imc.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;
import br.com.smartmonkey.imc.model.BodyMassIndex;


public class BD {
	private SQLiteDatabase bd;

	public BD(Context context){
		BDCore auxBd = new BDCore(context);
		bd = auxBd.getWritableDatabase();
	}


	public void save(BodyMassIndex bodyMassIndex){
		ContentValues valores = new ContentValues();
		valores.put("height", bodyMassIndex.getHeight());
		valores.put("weight", bodyMassIndex.getWeight());
		valores.put("gender", (bodyMassIndex.isGender())? 1 : 0);
		bd.insert("bdi", null, valores);
	}
	public List<BodyMassIndex> getBMIs(){
		List<BodyMassIndex> list = new ArrayList<BodyMassIndex>();
		String[] rows = new String[]{"_id", "height","weight","gender"};

		Cursor cursor = bd.query("bdi", rows, null, null, null, null, null);

		if(cursor.getCount() > 0){
			cursor.moveToFirst();

			do{

				BodyMassIndex bodyMassIndex = new BodyMassIndex();
				bodyMassIndex.setHeight(cursor.getFloat(1));
				bodyMassIndex.setWeight(cursor.getFloat(2));
				bodyMassIndex.setSex(cursor.getInt(3)==1);
				list.add(bodyMassIndex);

			}while(cursor.moveToNext());
		}

		return(list);
	}
}
