package br.com.smartmonkey.imc.controller;

import android.content.Context;
import android.util.Log;

import java.util.List;

import br.com.smartmonkey.imc.activiy.MainActivity;
import br.com.smartmonkey.imc.dao.BD;
import br.com.smartmonkey.imc.model.BodyMassIndex;

/**
 * Created by Gabriel on 28/07/2016.
 */
public class BMIControler {
    public void save(Context context, BodyMassIndex bmi){
        BD bd = new BD(context);
        bd.save(bmi);
    }
    public List<BodyMassIndex> getBMIs(Context context){
        BD bd = new BD(context);
        List<BodyMassIndex> list = bd.getBMIs();
        return list;
    }

}
